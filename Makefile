multifail: multifail.c
	$(CC) -O -o multifail multifail.c

multifail-asan: multifail.c
	$(CC) -O2 -fsanitize=address -o multifail-asan multifail.c

multifail-afl: multifail.c
	AFL_USE_ASAN=1 afl-clang -O2 -o multifail-afl multifail.c
