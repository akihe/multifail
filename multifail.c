#include <stdio.h>
#include <string.h>

int main() {
   int a, b;
   char command[32];
   char data[256];
   int admin = 0;
   int format = 0;

   for (;;) {
      printf(": ");
      fflush(stdout);    
      scanf("%s", &command);
      printf("> %s\n", command);
      if (strcmp(command, "print") == 0) {
         if (format) {
            printf(data); /* fail */
         } else {
            printf("%s", data);
         }
      } else if (strcmp(command, "reset") == 0) {
         scanf(" %s", data);
         printf("> set '%s'\n", data);
      } else if (strcmp(command, "set") == 0) {
         int pos, val;
         if(admin) {
            scanf(" %d", &pos);
            scanf(" %d", &val);
            printf("> data[%d] = %d\n", pos, val);
            data[pos] = val; /* fail */
         } else {
            printf("> no\n");
         }
      } else if (strcmp(command, "become") == 0) {
         scanf(" %s", command);
         admin = (strcmp(command, "yes") == 0) ? 1 : 0;
         printf("> admin = %d\n", admin);
      } else if (strcmp(command, "mode") == 0) {
         scanf(" %s", command);
         format = (strcmp(command, "format") == 0) ? 1 : 0;
         printf("> format = %d\n", format);
      } else if (strcmp(command, "get") == 0) {
         int pos;
         scanf(" %d", &pos);
         printf("> data[%d] = %d\n", pos, data[pos]);
      } else if (strcmp(command, "exit") == 0) {
         printf("> bye\n");
         break;
      } else {
         printf("?\n");
         return(1);
      }
   }
   return 0;
}
